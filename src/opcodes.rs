/// NOOP: 0x00
pub const NOOP: u8 = 0x00;

/// LD BC, d16
pub const LD_BC_D16: u8 = 0x01;

/// LD (BC), A
pub const LD_BC_A: u8 = 0x02;