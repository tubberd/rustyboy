use crate::opcodes::*;

/// Register union type.
/// Accessible as two byte parts or one 16 bit part.
pub union Register {
    pub first: u8,
    pub second: u8,
    pub whole: u16
}

/// Struct representing the GameBoy's CPU core.
pub struct Core {
    /// A + F Registers
    af: Register,
    /// B + C Registers
    bc: Register,
    /// D + E Registers
    de: Register,
    /// H + L Registers
    hl: Register,
    /// SP Register
    sp: Register,
    /// PC Register
    pc: Register,
    /// Counter variable for multi-cycle instructions
    cycle_skip: u8,
    /// Opcode jump table
    opcode_table: Vec<Box<dyn FnMut(&mut Core)>>,
    ram: Vec<u8>
}

impl Core {
    /// Creates a new CPU instance
    pub fn new() -> Self {
        let mut core = Self {
            af: Register {
                whole: 0
            },
            bc: Register {
                whole: 0
            },
            de: Register {
                whole: 0
            },
            hl: Register {
                whole: 0
            },
            sp: Register {
                whole: 0
            },
            pc: Register {
                whole: 0
            },
            cycle_skip: 0,
            ram: Vec::with_capacity(32768),
            opcode_table: Vec::with_capacity(255)
        };
        core.init_opcodes();
        core
    }

    /// Initializes the opcode table
    pub fn init_opcodes(&mut self) {
        self.opcode_table[NOOP as usize] = Box::new(Self::op_noop);
    }

    /// Writes a byte to the given address
    /// 
    /// # Params
    /// * `address`: The address to write to
    /// * `data`: The byte of date to write
    pub fn write_byte(&mut self, address: u16, data: u8) {

    }

    /// Reads a byte from the given address
    /// 
    /// # Params
    /// * `address`: The address to read from
    /// 
    /// # Returns
    /// The byte of data
    pub fn read_byte(&mut self, address: u16) -> u8 {
        0
    }

    /// Implementation for the NOOP (0x00) instruction
    pub fn op_noop(&mut self) {
        unsafe {
            self.pc.whole += 1;
        }
    }
}