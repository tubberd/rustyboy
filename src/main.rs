
/// Module containing the CPU core and related functionality
pub mod core;

/// Module containing the opcode constants
pub mod opcodes;

/// Module containing unit tests
#[cfg(test)]
pub mod test;

fn main() {
    println!("Hello, world!");
}
