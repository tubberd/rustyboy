use crate::core::*;

#[test]
fn test_register_bytesize() {
    assert_eq!(2, std::mem::size_of::<Register>());
}

#[test]
fn test_core_bytesize() {
    let opcode_size = std::mem::size_of::<Vec<Box<dyn FnMut(&mut Core)>>>();
    let register_size = std::mem::size_of::<Register>();
    let ram_size = std::mem::size_of::<Vec<u8>>();
    let mut total_size = (register_size * 6) + ram_size + opcode_size + 1;
    // Account for byte alignment on AMD64
    let align_diff = total_size % 4;
    total_size += 4 - align_diff;
    assert_eq!(total_size, std::mem::size_of::<Core>());
}